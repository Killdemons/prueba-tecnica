// Animations
AOS.init({
  anchorPlacement: 'top-left',
  duration: 1000
});

// Add your javascript here

var about = 2;
var skills = 1;
var experience = 1;
var education = 1;
var contact = 1;


function divAbout(){ 
  if(about==1){
   document.getElementById("about").style.display = "block";
   about = about + 1;
  } else{
   document.getElementById("about").style.display = "none";      
   about = 1;
  }   
}

function divSkills(){ 
  if(skills==1){
   document.getElementById("skills").style.display = "block";
  skills = skills + 1;
  } else{
   document.getElementById("skills").style.display = "none";      
   skills = 1;
  }   
}

function divExperience(){ 
  if(experience==1){
   document.getElementById("experience").style.display = "block";
  experience = experience + 1;
  } else{
   document.getElementById("experience").style.display = "none";      
   experience = 1;
  }   
}

function divEducation(){ 
  if(education==1){
   document.getElementById("education").style.display = "block";
  education = education + 1;
  } else{
   document.getElementById("education").style.display = "none";      
   education = 1;
  }   
}

function divContact(){ 
  if(contact==1){
   document.getElementById("contact").style.display = "block";
  contact = contact + 1;
  } else{
   document.getElementById("contact").style.display = "none";      
   contact = 1;
  }   
}