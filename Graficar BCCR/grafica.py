import requests
from datetime import date, timezone
from datetime import datetime
from xml.dom import minidom
import pandas as pd 
import matplotlib.pyplot as plt 
import numpy

cambio_combra = {}
cambio_venta = {}
length = 0

def cambio():
    tipo_cambio(317)
    tipo_cambio(318)
    graficar()

def tipo_cambio(indicador):
    global length
    fechainicio = "01/01/" + date.today().strftime('%Y')
    fechafinal = date.today().strftime('%d/%m/%Y')
    length = datetime.strptime(fechafinal, '%d/%m/%Y').date() - datetime.strptime(fechainicio, '%d/%m/%Y').date()
    length = length.days
    token = "AMHP2M2MO2"
    url = "https://gee.bccr.fi.cr/Indicadores/Suscripciones/WS/wsindicadoreseconomicos.asmx/ObtenerIndicadoresEconomicos?Indicador="+str(indicador)+"&FechaInicio="+fechainicio+"&FechaFinal="+fechafinal+"&Nombre=Bryan&SubNiveles=N&CorreoElectronico=b.camposvindas@hotmail.com&token="+token
    payload={}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    almacenar(response)

def almacenar(response):
    doc = minidom.parseString(response.text)
    for i in range(length+1):
        if int(doc.getElementsByTagName("COD_INDICADORINTERNO")[i].firstChild.data) == 317:
            cambio_combra[i] = {"indicador": int(doc.getElementsByTagName("COD_INDICADORINTERNO")[i].firstChild.data), "fecha" : datetime.fromisoformat(doc.getElementsByTagName("DES_FECHA")[i].firstChild.data).astimezone(timezone.utc).date().strftime('%d/%m/%Y'), "cambio" : float(doc.getElementsByTagName("NUM_VALOR")[i].firstChild.data)}
        elif int(doc.getElementsByTagName("COD_INDICADORINTERNO")[i].firstChild.data) == 318:
            cambio_venta[i] = {"indicador": int(doc.getElementsByTagName("COD_INDICADORINTERNO")[i].firstChild.data), "fecha" : datetime.fromisoformat(doc.getElementsByTagName("DES_FECHA")[i].firstChild.data).astimezone(timezone.utc).date().strftime('%d/%m/%Y'), "cambio" : float(doc.getElementsByTagName("NUM_VALOR")[i].firstChild.data)}



def graficar():
    fechas = [] #fechas
    compra = [] #317 Compra
    venta = [] #318 Venta
    rate_compra = [] # 1/tipo de cambio
    rate_venta = [] # 1/tipo de cambio
    for i in cambio_combra:
        if cambio_combra[i]["indicador"] == 317:
            compra.append(cambio_combra[i]["cambio"])
            if (cambio_combra[i]["fecha"] in fechas) == False:
                fechas.append(cambio_combra[i]["fecha"])
    for i in cambio_venta:
        if cambio_venta[i]["indicador"] == 318:
            venta.append(cambio_venta[i]["cambio"])
            if (cambio_venta[i]["fecha"] in fechas) == False:
                fechas.append(cambio_venta[i]["fecha"])
    for i in range(len(fechas)):
        rate_compra.append(1/compra[i])
        rate_venta.append(1/venta[i])
    media_compra = 0
    media_venta = 0
    for i in range(len(fechas)):
        media_compra += compra[i]
        media_venta += venta[i]
    print("La media de la compra es: " + str(float(numpy.mean(compra))) + "\nLa media de la venta es: " + str(float(numpy.mean(venta))))
    print("El promedio de la compra es: " + str(float(sum(compra)/len(compra))) + "\nEl promedio de la venta es: " + str(float(sum(venta)/len(venta))))
                
    df_days = pd.DataFrame( 
        { 'Fecha': fechas,  
        'Compra': compra,  
        'Venta': venta,
        'Rate compra': rate_compra,
        'Rate venta': rate_venta
        }) 
    
    ax = plt.gca() 
    
    df_days.plot( x = "Fecha" , y = "Compra", ax = ax )
    df_days.plot( x = "Fecha" , y = "Venta" , ax = ax )
    df_days.plot( x = "Fecha" , y = "Rate compra" , ax = ax ) 
    df_days.plot( x = "Fecha" , y = "Rate venta" , ax = ax ) 

cambio()


